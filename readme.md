# Tasks fe-school-testing
Hi, here are my resolve of tasks for Frontend School at T-Systems.

To get a tasks solution insert this command in terminal of your code editor:

- git clone https://gitlab.com/bahir24/fe-school-testing.git

## 1 Page from JPEG
To launch a page in sequence insert:

  - cd fe-school-testing/1st_task
  - git init
  - yarn init -y
  - yarn add webpack -D
  - yarn run dev

  and open in browser link "localhost:8080"

To get a ready bundle open folder "dist".
  
## 2 Maze

Files with solving placed in 2nd_task folder